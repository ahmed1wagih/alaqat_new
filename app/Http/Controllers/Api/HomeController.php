<?php

namespace App\Http\Controllers\Api;

use App\Models\Order;
use App\Models\OrderImage;
use App\Models\Pack;
use App\Models\Service;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function index()
    {
        $services = Service::select('id','title','desc','color')->get();
        return response()->json(['services' => $services]);
    }


    public function service_details($id)
    {
        $service = Service::where('id', $id)->select('id','title','desc','color')->first();
        $packs = Pack::where('service_id', $service->id)->select('id','title','desc','price','color')->get();
        return response()->json(['service' => $service, 'packs' => $packs]);
    }


    public function get_celebs()
    {
        $celebs = User::where('role', 'celeb')->where('active', 1)->select('id','name','title','image')->get();

        foreach($celebs as $celeb)
        {
            $celeb['image'] = '/public/users/'.$celeb->image;
        }

        return response()->json(['celebs' => $celebs]);
    }


    public function order(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'name' => 'required',
                'desc' => 'required',
                'celebs_ids' => 'required|array',
                'from' => 'required|date',
                'to' => 'required|date',
                'images' => 'required',
                'images.*' => 'image'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
        }

        $order = Order::create
        (
            [
                'user_id' => $request->user_id,
                'name' => $request->name,
                'desc' => $request->desc,
                'celebs_ids' => serialize($request->celebs_ids),
                'from' => $request->from,
                'to' => $request->to,
            ]
        );

        foreach($request->images as $image)
        {
            $name = unique_file($image->getClientOriginalName());
            $image->move(base_path().'/public/orders/',$name);

            OrderImage::create
            (
                [
                    'order_id' => $order->id,
                    'image' => $name
                ]
            );
        }

        return response()->json(['status' => 'success', 'msg' => 'ordered successfully']);
    }


    public function celebs_search(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'text' => 'required'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
        }

        $celebs = User::where('role', 'celeb')->where('name', 'like', '%'.$request->text.'%')->orWhere('title', 'like', '%'.$request->text.'%')->select('id','name','title','image')->get();

        foreach($celebs as $celeb)
        {
            $celeb['image'] = '/public/users/'.$celeb->image;
        }

        return response()->json(['result' => $celebs]);
    }


    public function profile($id,$user_id)
    {
        $services = Service::select('id','title','desc')->get();

        foreach($services as $service)
        {
            $service['applied'] = $service->check_status($service->id,$user_id);
        }
        $user = User::where('id', $id)->select('id','name','title','desc','image','facebook_link','youtube_link','twitter_link','insta_link','snapchaat_link')->first();

        $user['name'] = isset($user->name) ? $user->name : '';
        $user['title'] = isset($user->title) ? $user->title : '';
        $user['desc'] = isset($user->desc) ? $user->desc : '';
        $user['facebook_link'] = isset($user->facebook_link) ? $user->facebook_link : '';
        $user['youtube_link'] = isset($user->youtube_link) ? $user->youtube_link : '';
        $user['twitter_link'] = isset($user->twitter_link) ? $user->twitter_link : '';
        $user['snapchat_link'] = isset($user->snapchat_link) ? $user->snapchat_link : '';
        $user['insta_link'] = isset($user->insta_link) ? $user->insta_link : '';

        $user['image'] = '/public/users/'.$user->image;

        return response()->json(['services' => $services ,'user' => $user]);
    }


    public function update_profile(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'name' => 'sometimes',
                'desc' => 'sometimes',
                'facebook_link' => 'sometimes',
                'youtube_link' => 'sometimes',
                'twitter_link' => 'sometimes',
                'insta_link' => 'sometimes',
                'snapchat_link' => 'sometimes',
                'image' => 'sometimes'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
        }

        $user = User::where('id', $request->user_id)->update
        (
            [
                'name' => $request->name,
                'desc' => $request->desc,
                'facebook_link' => $request->facebook_link,
                'twitter_link' => $request->twitter_link,
                'youtube_link' => $request->youtube_link,
                'snapchat_link' => $request->snapchat_link,
                'insta_link' => $request->insta_link,
            ]
        );

        if($request->image != null)
        {
            $image = unique_file($request->image->getClientOriginalName());
            $request->image->move(base_path().'/users/', $image);

            $user->image = $image;
            $user->save();
        }

        return response()->json(['status' => 'success', 'msg' => 'profile updated']);
    }


    public function subscribe(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'pack_id' => 'required|exists:packs,id'
            ]
        );

        if($validator->fails())
        {
            return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
        }

        Subscription::create
        (
            [
                'user_id' => $request->user_id,
                'pack_id' => $request->pack_id
            ]
        );

        return response()->json(['status' => 'success', 'msg' => 'applied successfully']);
    }
}
