@extends('admin.layouts.app')
@section('content')
    <!-- PAGE CONTENT WRAPPER -->

    <div class="page-content-wrap" style="margin-top: 10px;">

        <div class="row">
            <div class="col-md-3">

                <div class="widget widget-default widget-carousel">
                    <div class="owl-carousel" id="owl-example">
                        <div>
                            <div class="widget-title">كل الأعضاء</div>
                            <div class="widget-subtitle">{{\Carbon\Carbon::now()}}</div>
                            <div class="widget-int">{{$users}}</div>
                        </div>
                        <div>
                            <div class="widget-title">أعضاء جدد</div>
                            <div class="widget-subtitle">منذ أسبوع</div>
                            <div class="widget-int">{{$latest_users}}</div>
                        </div>
                    </div>
                    <div class="widget-controls">
                        <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                    </div>
                </div>
                <!-- END WIDGET SLIDER -->
            </div>

            <div class="col-md-3">

                <div class="widget widget-default widget-carousel">
                    <div class="owl-carousel" id="owl-example">
                        <div>
                            <div class="widget-title">كل المشاهير</div>
                            <div class="widget-subtitle">{{\Carbon\Carbon::now()}}</div>
                            <div class="widget-int">{{$celebs}}</div>
                        </div>
                        <div>
                            <div class="widget-title">مشاهير جدد</div>
                            <div class="widget-subtitle">منذ أسبوع</div>
                            <div class="widget-int">{{$latest_celebs}}</div>
                        </div>
                    </div>
                    <div class="widget-controls">
                        <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                    </div>
                </div>
                <!-- END WIDGET SLIDER -->
            </div>

            <div class="col-md-3">

                <!-- START WIDGET MESSAGES -->
                <div class="widget widget-default widget-item-icon" onclick="location.href='/admin/services';">
                    <div class="widget-item-left">
                        <span class="fa fa-cogs"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 60px;">
                        <div class="widget-int num-count">{{$services}}</div>
                        <div class="widget-title">عدد الخدمات</div>
                        <div class="widget-subtitle">في التطبيق</div>
                    </div>
                    <div class="widget-controls">
                        <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                    </div>
                </div>
                <!-- END WIDGET MESSAGES -->

            </div>

            <div class="col-md-3">

                <!-- START WIDGET MESSAGES -->
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fa fa-cogs"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 60px;">
                        <div class="widget-int num-count">{{$packs}}</div>
                        <div class="widget-title">عدد الباقات</div>
                        <div class="widget-subtitle">في التطبيق</div>
                    </div>
                    <div class="widget-controls">
                        <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                    </div>
                </div>
                <!-- END WIDGET MESSAGES -->

            </div>

            <div class="col-md-3">

                <!-- START WIDGET MESSAGES -->
                <div class="widget widget-default widget-item-icon" onclick="location.href='/admin/subscriptions/awaiting';">
                    <div class="widget-item-left">
                        <span class="fa fa-cogs"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 60px;">
                        <div class="widget-int num-count">{{$subs}}</div>
                        <div class="widget-title">عدد الإشتراكات</div>
                        <div class="widget-subtitle">في التطبيق</div>
                    </div>
                    <div class="widget-controls">
                        <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                    </div>
                </div>
                <!-- END WIDGET MESSAGES -->

            </div>

            <div class="col-md-3">

                <!-- START WIDGET MESSAGES -->
                <div class="widget widget-default widget-item-icon" onclick="location.href='/admin/orders/awaiting';">
                    <div class="widget-item-left">
                        <span class="fa fa-cogs"></span>
                    </div>
                    <div class="widget-data" style="margin-right: 60px;">
                        <div class="widget-int num-count">{{$orders}}</div>
                        <div class="widget-title">عدد الطلبات</div>
                        <div class="widget-subtitle">في التطبيق</div>
                    </div>
                    <div class="widget-controls">
                        <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                    </div>
                </div>
                <!-- END WIDGET MESSAGES -->
            </div>
        </div>
        <!-- END WIDGETS -->


@endsection
