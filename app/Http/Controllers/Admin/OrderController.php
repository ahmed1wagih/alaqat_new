<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index($status)
    {
        $orders = Order::where('status', $status)->paginate(50);
        return view('admin.orders.index', compact('orders','status'));
    }


    public function change_state(Request $request)
    {
        $this->validate($request,
            [
                'order_id' => 'required|exists:orders,id',
                'status' => 'required|in:approved,awaiting,declined'
            ]
        );

        $order = Order::where('id', $request->order_id)->first();
            $order->status = $request->status;
        $order->save();

        return back()->with('success', 'تم تغيير الحالة بنجاح');
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'order_id' => 'required|exists:orders,id'
            ]
        );

        Order::where('id', $request->order_id)->delete();

        return back()->with('success', 'تم الحذف بنجاح');
    }
}
