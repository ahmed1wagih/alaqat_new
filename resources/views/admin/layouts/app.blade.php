<!DOCTYPE html>
<html lang="en">
<head>
    <!-- META SECTION -->
    <title>علاقات - لوحة التحكم</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="{{asset('admin/img/favicon.png')}}" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('admin/css/theme-default_rtl.css')}}"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{asset('admin/css/rtl.css')}}"/>
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery/jquery.min.js')}}"></script>
    <!-- START PLUGINS -->
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery/jquery-ui.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap.min.js')}}"></script>
    <!-- END PLUGINS -->
    <!-- EOF CSS INCLUDE -->

    <style>
        .required
        {
            color: red;
        }

        .tiny_img
        {
            width: 50px;
            height: 50px;
            border-radius: 100%;
        }
    </style>
</head>
<body>
<!-- START PAGE CONTAINER -->
<div class="page-container page-mode-rtl page-content-rtl">

    <!-- START PAGE SIDEBAR -->
    <div class="page-sidebar page-sidebar-fixed scroll" style="height: 0px !important">
        <!-- START X-NAVIGATION -->
        <ul class="x-navigation">
            <li class="xn-logo">
                <a href="/admin/dashboard">Halls</a>
            </li>
            <li class="xn-profile">

                <div class="profile">
                    <div class="profile-image">
                        <img src="{{asset('admin/img/favicon.png')}}" alt="علاقات"/>
                    </div>
                </div>
            </li>
            <li>
                <a href="/admin/dashboard"><span class="xn-text">الرئيسية</span> <span class="fa fa-home"></span></a>
            </li>
            <li class="xn-openable @if(Request::is('admin/users/*')) active @endif" >
                <a href="#"><span class="xn-text">المستخدمين</span> <span class="fa fa-group"></span></a>
                <ul>
                    <li @if(Request::is('admin/users/admins ')) class="active" @endif>
                        <a href="/admin/users/admins"><span class="xn-text">المديرين</span><span class="fa fa-user-secret"></span></a>
                    </li>
                    <li @if(Request::is('admin/users/celebs')) class="active" @endif>
                        <a href="/admin/users/celebs"><span class="xn-text">المشاهير</span><span class="fa fa-star"></span></a>
                    </li>
                    <li @if(Request::is('admin/users/users')) class="active" @endif>
                        <a href="/admin/users/users"><span class="xn-text">المستخدمين</span><span class="fa fa-user"></span></a>
                    </li>
                    <li @if(Request::is('admin/users/blocked')) class="active" @endif>
                        <a href="/admin/users/blocked"><span class="xn-text">المستخدمين المحظورين</span><span class="fa fa-user-o"></span></a>
                    </li>
                </ul>
            </li>
            <li @if(Request::is('admin/services') || Request::is('admin/service/*') || Request::is('admin/pack/*')) class="active" @endif>
                <a href="/admin/services"><span class="xn-text">الخدمات</span> <span class="fa fa-handshake-o"></span></a>
            </li>
            <li class="xn-openable @if(Request::is('admin/subscriptions/*')) active @endif" >
                <a href="#"><span class="xn-text">الإشتراكات</span> <span class="fa fa-files-o"></span></a>
                <ul>
                    <li @if(Request::is('admin/subscriptions/approved')) class="active" @endif>
                        <a href="/admin/subscriptions/approved"><span class="xn-text">تم الموافقة عليها</span><span class="fa fa-check-circle"></span></a>
                    </li>
                    <li @if(Request::is('admin/subscriptions/awaiting')) class="active" @endif>
                        <a href="/admin/subscriptions/awaiting"><span class="xn-text">في إنتظار الموافقة</span><span class="fa fa-circle-o"></span></a>
                    </li>
                    <li @if(Request::is('admin/subscriptions/declined')) class="active" @endif>
                        <a href="/admin/subscriptions/declined"><span class="xn-text">مرفوضة</span><span class="fa fa-times-circle"></span></a>
                    </li>
                </ul>
            </li>
            <li class="xn-openable @if(Request::is('admin/orders/*')) active @endif" >
                <a href="#"><span class="xn-text">الطلبات</span> <span class="fa fa-files-o"></span></a>
                <ul>
                    <li @if(Request::is('admin/orders/approved')) class="active" @endif>
                        <a href="/admin/orders/approved"><span class="xn-text">تم الموافقة عليها</span><span class="fa fa-check-circle"></span></a>
                    </li>
                    <li @if(Request::is('admin/orders/awaiting')) class="active" @endif>
                        <a href="/admin/orders/awaiting"><span class="xn-text">في إنتظار الموافقة</span><span class="fa fa-circle-o"></span></a>
                    </li>
                    <li @if(Request::is('admin/orders/declined')) class="active" @endif>
                        <a href="/admin/orders/declined"><span class="xn-text">مرفوضة</span><span class="fa fa-times-circle"></span></a>
                    </li>
                </ul>
            </li>
            <li @if(Request::is('admin/settings') || Request::is('admin/settings/*')) class="active" @endif>
                <a href="/admin/settings"><span class="xn-text">الإعدادات</span> <span class="fa fa-cogs"></span></a>
            </li>
        </ul>
        <!-- END X-NAVIGATION -->
    </div>
    <!-- END PAGE SIDEBAR -->

    <!-- PAGE CONTENT -->
    <div class="page-content">

        <!-- START X-NAVIGATION VERTICAL -->
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            <!-- POWER OFF -->
            <li class="xn-icon-button last">
                <a href="/logout" title="تسجيل الخروج"><span class="fa fa-power-off"></span></a>
            </li>
            <!-- END POWER OFF -->
        </ul>
        <!-- END X-NAVIGATION VERTICAL -->

     @yield('content')





<!-- START PRELOADS -->
<audio id="audio-alert" src="{{asset('admin/audio/alert.mp3')}}" preload="auto"></audio>
<audio id="audio-fail" src="{{asset('admin/audio/fail.mp3')}}" preload="auto"></audio>
<!-- END PRELOADS -->


<!-- THIS PAGE PLUGINS -->
<script type='text/javascript' src="{{asset('admin/js/plugins/icheck/icheck.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

<script type="text/javascript" src="{{asset('admin/js/plugins/owl/owl.carousel.min.js')}}"></script>
<!-- END PAGE PLUGINS -->

<!-- START TEMPLATE -->
<script type="text/javascript" src="{{asset('admin/js/plugins.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/actions.js')}}"></script>


<script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap-select.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/tagsinput/jquery.tagsinput.min.js')}}"></script>
<!-- END THIS PAGE PLUGINS -->
<!-- END SCRIPTS -->
</body>
</html>






