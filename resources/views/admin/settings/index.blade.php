@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li class="active">الإعدادات</li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12 col-xs-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="rtl_th">إسم الإعداد</th>
                        <th class="rtl_th">القيمة</th>
                        <th class="rtl_th">الإجراء المتخذ</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($settings as $setting)
                    <tr>
                        <td>{{$setting->name}}</td>
                        <td>{!! $setting->value !!}</td>
                        <td>
                            <a href="/admin/setting/{{$setting->id}}/edit" title="تعديل" class="buttons"><button class="btn btn-warning btn-condensed"><i class="fa fa-edit"></i></button></a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            </div>
            </div>
            </div>
        </div>
    </div>

@endsection
