@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li><a href="/admin/settings">الإعدادات</a></li>
        <li class="active">تعديل</li>
    </ul>
    <!-- END BREADCRUMB -->
{{--{{dd($errors)}}--}}
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="/admin/setting/update" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <strong>
                                   تعديل إعداد
                                </strong>
                            </h3>
                        </div>
                        <div class="panel-body">
                                <div class="form-group {{ $errors->has('value') ? ' has-error' : '' }}">
                                    <label class="col-md-3 col-xs-12 control-label">{{$setting->name}}</label>
                                    <div class="col-md-6 col-xs-12">
                                            @if($setting->id == 2 || $setting->id == 3)
                                            <div class="input-group" >
                                                <span class="input-group-addon"><span class="fa fa-image"></span></span>
                                                <a class="fileinput"><span></span><input class="fileinput" name="value" id="filename1" type="file"></a>
                                            </div>
                                            <br/>
                                            <img src="{{asset('/settings/'.$setting->value)}}" width="300px" height="300px" style="border: black solid 1px;">
                                        @elseif($setting->id == 4)
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-cog"></span></span>
                                                <textarea class="summernote" name="value" rows="5">{{$setting->value}}</textarea>
                                            </div>
                                        @else
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-cog"></span></span>
                                                <input type="text" class="form-control" name="value" value="{{$setting->value}}"/>
                                            </div>
                                        @endif
                                        @include('admin.layouts.error', ['input' => 'value'])
                                    </div>
                                </div>
                            <input type="hidden" value="{{$setting->id}}" name="setting_id">
                        </form>
                        </div>
                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                                تعديل
                            </button>
                        </div>
                    </div>
                </form>

            </div>

    <!-- THIS PAGE PLUGINS -->
    <script type='text/javascript' src="{{asset('admin/js/plugins/icheck/icheck.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/summernote/summernote.js')}}"></script>
    <!-- END PAGE PLUGINS -->
@endsection
