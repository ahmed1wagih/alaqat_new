@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li class="active">الخدمات</li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12 col-xs-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="/admin/service/create">
            <button type="button" class="btn btn-info">أضف خدمة</button>
            </a>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="rtl_th">العنوان</th>
                        <th class="rtl_th">الوصف</th>
                        <th class="rtl_th">عدد الباقات</th>
                        <th class="rtl_th">الإجراء المتخذ</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($services as $service)
                    <tr>
                        <td>{{$service->title}}</td>
                        <td>{{$service->desc}}</td>
                        <td>{{$service->packs->count()}}</td>
                        <td>
                            <a href="/admin/service/{{$service->id}}/packs" title="مشاهدة الباقات" class="buttons"><button class="btn btn-info btn-condensed"><i class="fa fa-eye"></i></button></a>
                            <a href="/admin/service/{{$service->id}}/edit" title="تعديل" class="buttons"><button class="btn btn-warning btn-condensed"><i class="fa fa-edit"></i></button></a>
                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-warning-{{$service->id}}" title="حذف"><i class="fa fa-trash-o"></i></button>
                        </td>
                    </tr>
                    <!-- danger with sound -->
                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-warning-{{$service->id}}">
                        <div class="mb-container">
                            <div class="mb-middle warning-msg alert-msg">
                                <div class="mb-title"><span class="fa fa-times"></span> الرجاء الإنتباه</div>
                                <div class="mb-content">
                                   <p>أنت علي وشك أن تحذف هذه الخدمة و لن تستطيع إسترجاع بياناتها مره أخري,مثل الباقات و الإشتراكات و ما إلي خلافه,هل أنت متأكد ؟</p>
                                </div>
                                <div class="mb-footer buttons">
                                    <form method="post" action="/admin/service/delete" class="buttons">
                                        {{csrf_field()}}
                                        <input type="hidden" name="service_id" value="{{$service->id}}">
                                        <button class="btn btn-default btn-lg pull-right">حذف</button>
                                    </form>
                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">إلغاء</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end danger with sound -->
                    @endforeach
                    </tbody>

                </table>
                {{$services->links()}}
            </div>
            </div>
            </div>
            </div>
        </div>
    </div>

@endsection
