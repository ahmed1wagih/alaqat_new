@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li><a href="/admin/services">الخدمات</a></li>
        @if(isset($pack)) <li><a>{{$pack->service->title}}</a></li> @else <li><a>{{$service->title}}</a></li>  @endif
        <li class="active">
            @if(isset($pack))
                تعديل باقة
            @else
                إضافة باقة
            @endif
        </li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" @if(isset($pack)) action="/admin/pack/update" @else action="/admin/pack/store" @endif>
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <strong>
                                   @if(isset($pack))
                                        تعديل باقة
                                    @else
                                        إضافة باقة
                                   @endif
                                </strong>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">العنوان</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="title" value="{{isset($pack) ? $pack->title : old('title')}}"/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'title'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('price') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">السعر</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-dollar"></span></span>
                                        <input type="number" class="form-control" name="price" value="{{isset($pack) ? $pack->price : old('price')}}"/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'price'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('details') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">التفاصيل</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea class="summernote" name="desc" value="{{old('desc')}}">{{isset($pack) ? $pack->desc : old('desc')}}</textarea>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'desc'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('color') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">اللون (من 1 إلي 20 )</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-eyedropper"></span></span>
                                        <input type="number" class="form-control" name="color" value="{{isset($pack) ? $pack->color : old('color')}}"/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'color'])
                                </div>
                            </div>

                            @if(isset($service))
                                <input type="hidden" name="service_id" value="{{$service->id}}">
                            @endif

                            @if(isset($pack))
                            <input type="hidden" name="pack_id" value="{{$pack->id}}">
                            @endif
                </form>
                        </div>
                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                                @if(isset($pack))
                                    تعديل
                                @else
                                    إضافة
                                @endif
                            </button>
                        </div>
                    </div>
    </div>

    <!-- THIS PAGE PLUGINS -->
    <script type='text/javascript' src="{{asset('admin/js/plugins/icheck/icheck.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/summernote/summernote.js')}}"></script>
    <!-- END PAGE PLUGINS -->


@endsection
