<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pack;
use App\Models\Service;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function admins()
    {
        $users = User::where('role', 1)->where('active', 1)->where('id', '!=', 1)->paginate(50);
        return view('admin.users.index', compact('users'));
    }


    public function celebs()
    {
        $users = User::where('role', 2)->where('active', 1)->paginate(50);
        return view('admin.users.index', compact('users'));
    }


    public function users()
    {
        $users = User::where('role', 3)->where('active', 1)->paginate(50);
        return view('admin.users.index', compact('users'));
    }


    public function blocked()
    {
        $users = User::where('active', 0)->paginate(50);
        return view('admin.users.index', compact('users'));
    }


    public function create()
    {
        return view('admin.users.single');
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'role' => 'required|in:admin,celeb,user',
                'name' => 'nullable',
                'title' => 'nullable',
                'desc' => 'nullable',
                'email' => 'required|unique:users,email',
                'username' => 'required|unique:users,username',
                'password' => 'required|confirmed',
                'image' => 'nullable|image',
                'facebook_link' => 'nullable|unique:users,facebook_link',
                'youtube_link' => 'nullable|unique:users,youtube_link',
                'twitter_link' => 'nullable|unique:users,twitter_link',
                'snapchat_link' => 'nullable|unique:users,snapchat_link',
                'insta_link' => 'nullable|unique:users,insta_link',
            ],
            [
                'role.required' => 'برجاء إختيار دور نوع المستخدم',
                'email.required' => 'البريد الإلكتروني مطلوب',
                'email.unique' => 'البريد الإلكتروني موجود من قبل',
                'username.required' => 'إسم الدخول مطلوب',
                'username.unique' => 'إسم الدخول موجود من قبل',
                'password.required' => 'كلمة المرور مطلوبة',
                'password.confirmed' => 'كلمات المرور غير متطابقة',
                'image.image' => 'صيغة الصورة غير صحيحة',
                'facebook_link.unique' => 'عنوان فيسبوك موجود من قبل',
                'youtube_link.unique' => 'عنوان يوتيوب موجود من قبل',
                'twitter_link.unique' => 'عنوان تويتر موجود من قبل',
                'snapchat_link.unique' => 'عنوان سناب شات موجود من قبل',
                'insta_link.unique' => 'عنوان إنستاجرام موجود من قبل',
            ]
        );

        $user = User::create($request->except('password','image'));

        if($request->image)
        {
            $image = unique_file($request->image->getClientOriginalName());
            $request->image->move(base_path().'/public/users/',$image);
            $user->image = $image;
        }

        $user->password = Hash::make($request->password);
        $user->save();
        return redirect('/admin/users/'.$user->role.'s')->with('success', 'تمت الإضافة بنجاح');
    }


    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.users.single', compact('user'));
    }


    public function update(Request $request)
    {

        $this->validate($request,
            [
                'user_id' => 'required|exists:users,id',
                'name' => 'nullable',
                'title' => 'nullable',
                'desc' => 'nullable',
                'email' => 'nullable|unique:users,email,'.$request->user_id,
                'password' => 'nullable|confirmed',
                'image' => 'nullable|image',
                'facebook_link' => 'nullable|unique:users,facebook_li,nk,'.$request->user_id,
                'youtube_link' => 'nullable|unique:users,youtube_link'.$request->user_id,
                'twitter_link' => 'nullable|unique:users,twitter_link,'.$request->user_id,
                'snapchat_link' => 'nullable|unique:users,snapchat_link,'.$request->user_id,
                'insta_link' => 'nullable|unique:users,insta_link,'.$request->user_id,
            ],
            [
                'role.required' => 'برجاء إختيار دور نوع المستخدم',
                'email.required' => 'البريد الإلكتروني مطلوب',
                'email.unique' => 'البريد الإلكتروني موجود من قبل',
                'password.confirmed' => 'كلمات المرور غير متطابقة',
                'image.image' => 'صيغة الصورة غير صحيحة',
                'facebook_link.unique' => 'عنوان فيسبوك موجود من قبل',
                'youtube_link.unique' => 'عنوان يوتيوب موجود من قبل',
                'twitter_link.unique' => 'عنوان تويتر موجود من قبل',
                'snapchat_link.unique' => 'عنوان سناب شات موجود من قبل',
                'insta_link.unique' => 'عنوان إنستاجرام موجود من قبل',
            ]
        );

        User::where('id',$request->user_id)->update
        (
            [
                'name' => $request->name,
                'title' => $request->title,
                'desc' => $request->desc,
                'email' => $request->email,
                'facebook_link' => $request->facebook_link,
                'youtube_link' => $request->youtube_link,
                'twitter_link' => $request->twitter_link,
                'snapchat_link' => $request->snapchat_link,
                'insta_link' => $request->insta_link,
            ]
        );

        if($request->image != NULL)
        {
            $user = User::find($request->user_id);
                $image = unique_file($request->image->getClientOriginalName());
                $request->image->move(base_path().'/public/users/', $image);
                $user->image = $image;
            $user->save();
        }


        return redirect('/admin/users/users')->with('success', 'تم التعديل بنجاح');

    }



    public function change_state(Request $request)
    {
        $this->validate($request,
            [
                'user_id' => 'required|exists:users,id'
            ]
        );

        $user = User::find($request->user_id);
            if($user->active == 0)
            {
                $user->active = 1;
            }
            else
            {
                $user->active = 0;
            }
        $user->save();

        return back()->with('success', 'تمت العملية بنجاح');
    }


    public function pack_assign_view($id)
    {
        $user = User::find($id);
        $services  = Service::all();

        return view('admin.users.assign', compact('user','services'));
    }


    public function pack_assign(Request $request)
    {

        $this->validate($request,
            [
                'user_id' => 'required|exists:users,id',
                'pack_id' => 'required|exists:packs,id',
                'type' => 'required|in:recipt,credit',
                'date' => 'date|nullable'
            ]
        );

        $user = User::find($request->user_id);
        $pack = Pack::find($request->pack_id);


        if($request->type == 'credit' && $user->credit < $pack->price)
        {
            return back()->with('error', 'عفواً,رصيد المتسخدم غير كافي');
        }

        $sub = Subscription::updateOrCreate
        (
            [
                'user_id' => $request->user_id,
                'pack_id' => $request->pack_id,
                'status' => 'approved',
                'type' => $request->type
            ]
        );

        if($request->no_date == 'on')
        {
            $sub->expire_at = NULL;
        }
        else
        {
            $sub->expire_at = $request->date;
        }

        $sub->save();

        if($request->type == 'credit')
        {
            $user->credit = $user->credit - $pack->price;
            $user->save();
        }


        return redirect('/admin/users/users')->with('success', 'تم تعيين الباقة للمتسخدم بنجاح');
    }


    public function credit_assign_view($id)
    {
        $user = User::find($id);
        return view('admin.users.credit', compact('user'));
    }


    public function credit_assign(Request $request)
    {

        $this->validate($request,
            [
                'user_id' => 'required|exists:users,id',
                'credit' => 'required|integer'
            ],
            [
                'credit.integer' => 'الرصيد لا بد أن يكون رقم'
            ]
        );

        $user = User::find($request->user_id);
            $user->credit = $request->credit;
        $user->save();

        return redirect('/admin/users/users')->with('success', 'تم تعديل الرصيد للمتسخدم بنجاح');
    }


    public function get_packs($id)
    {
        $packs = Pack::where('service_id', $id)->get();
        return response()->json($packs);
    }



    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'user_id' => 'required|exists:users,id'
            ]
        );

        User::where('id', $request->user_id)->delete();

        return back()->with('success', 'تمت الحذف بنجاح');
    }
}
