@extends('admin.layouts.app')
@section('content')
    <style>
        .celeb
        {
            width: 50px; height: 50px; border-radius: 100%; margin-bottom: 2px;
        }
    </style>
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>الطلبات</li>
        <li class="active">
            @if($status == 'approved')
                تم الموافقة عليها
            @elseif($status == 'awaiting')
                في إنتظار الموافقة
            @else
                مرفوضة
            @endif
        </li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12 col-xs-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="rtl_th">#</th>
                        <th class="rtl_th">إسم المستخدم</th>
                        <th class="rtl_th">الإسم</th>
                        <th class="rtl_th">الوصف</th>
                        <th class="rtl_th">المشاهير</th>
                        <th class="rtl_th">من - إلي</th>
                        <th class="rtl_th">الحالة</th>
                        <th class="rtl_th">الإجراء المتخذ</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                    <tr>
                        <td>{{$order->id}}</td>
                        <td>{{isset($order->user->name) ? $order->user->name : $order->user->username}}</td>
                        <td>{{$order->name}}</td>
                        <td>{{$order->desc}}</td>
                        <td>
                            @forelse(unserialize($order->celebs_ids) as $id)
                                <img class="celeb" title="{{\App\Models\Order::get_celeb($id)->name}}" src="/users/{{\App\Models\Order::get_celeb($id)->image}}"><br/>
                            @empty
                                لا يوجد
                            @endforelse
                        </td>
                        <td>{{$order->from}} - {{$order->to}}</td>
                        <td>
                            @if($order->status == 'approved')
                                تمت الموافقة
                            @elseif($order->status == 'awaiting')
                                في الإنتظار
                            @else
                                مرفوض
                            @endif
                        </td>
                        <td>
                            @if($order->status != 'approved')
                                <form method="post" action="/admin/order/change_state" class="buttons">
                                    {{csrf_field()}}
                                    <input type="hidden" name="order_id" value="{{$order->id}}">
                                    <input type="hidden" name="status" value="approved">
                                    <button class="btn btn-success btn-condensed" title="موافقة"><i class="fa fa-check-square"></i></button>
                                </form>
                            @endif
                            <form method="post" action="/admin/order/change_state" class="buttons">
                                {{csrf_field()}}
                                <input type="hidden" name="order_id" value="{{$order->id}}">
                                <input type="hidden" name="status" value="declined">
                                <button class="btn btn-danger btn-condensed" title="رفض"><i class="fa fa-minus-circle"></i></button>
                            </form>

                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-warning-{{$order->id}}" title="حذف"><i class="fa fa-trash-o"></i></button>
                        </td>
                    </tr>
                    <!-- danger with sound -->
                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-warning-{{$order->id}}">
                        <div class="mb-container">
                            <div class="mb-middle warning-msg alert-msg">
                                <div class="mb-title"><span class="fa fa-times"></span> الرجاء الإنتباه</div>
                                <div class="mb-content">
                                   <p>أنت علي وشك أن تحذف هذا الإشتراك و لن تستطيع إسترجاع بياناته مره أخري,هل أنت متأكد ؟</p>
                                </div>
                                <div class="mb-footer buttons">
                                    <form method="post" action="/admin/order/delete" class="buttons">
                                        {{csrf_field()}}
                                        <input type="hidden" name="order_id" value="{{$order->id}}">
                                        <button class="btn btn-default btn-lg pull-right">حذف</button>
                                    </form>
                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">إلغاء</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end danger with sound -->
                    @endforeach
                    </tbody>

                </table>
                {{$orders->links()}}
            </div>
            </div>
            </div>
            </div>
        </div>
    </div>

@endsection
