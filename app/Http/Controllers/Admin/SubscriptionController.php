<?php

namespace App\Http\Controllers\Admin;

use App\Models\Subscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriptionController extends Controller
{
    public function index($status)
    {
        $subs = Subscription::where('status', $status)->paginate(50);
        return view('admin.subscriptions.index', compact('subs', 'status'));
    }


    public function change_state(Request $request)
    {
        $this->validate($request,
            [
                'sub_id' => 'required|exists:subscriptions,id',
                'status' => 'required|in:approved,awaiting,declined'
            ]
        );

        $sub = Subscription::where('id', $request->sub_id)->first();
            $sub->status = $request->status;
        $sub->save();

        return back()->with('success', 'تم تغيير الحالة بنجاح');
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'sub_id' => 'required|exists:subscriptions,id'
            ]
        );

        Subscription::where('id', $request->sub_id)->delete();

        return back()->with('success', 'تم الحذف بنجاح');
    }
}
