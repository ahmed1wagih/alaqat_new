<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id','name','desc','from','to','celebs_ids'
    ];


    public static function get_celeb($id)
    {
        $celeb = User::find($id);
        return $celeb;
    }


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
