@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li><a>مستخدمين التطبيق</a></li>
        <li class="active">
            @if(isset($user))
                تعديل مستخدم
            @else
                إضافة مستخدم
            @endif
        </li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" @if(isset($user)) action="/admin/user/update" @else action="/admin/user/store" @endif enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <strong>
                                    @if(isset($user))
                                        تعديل مستخدم
                                    @else
                                        إضافة مستخدم
                                    @endif
                                </strong>
                            </h3>
                        </div>
                        <div class="panel-body">

                            @if(!$user)
                            <div class="form-group {{ $errors->has('role') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label"><span class="required">*</span> الدور</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                            <select class="form-control select" name="role" required aria-required="">
                                                <option disabled selected>إختر نوع المستخدم</option>
                                                <option value="admin">مدير</option>
                                                <option value="celeb">مشهور</option>
                                                <option value="user">مستخدم</option>
                                            </select>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'role'])
                                </div>
                            </div>
                            @endif

                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الإسم</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="name" value="{{isset($user) ? $user->name : old('name')}}"/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'name'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الوظيفة</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="title" value="{{isset($user) ? $user->title : old('title')}}"/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'title'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('desc') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الوصف</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="desc" value="{{isset($user) ? $user->desc : old('desc')}}"/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'desc'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">البريد الإلكتروني</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
                                        <input type="email" class="form-control" name="email" value="{{isset($user) ? $user->email : old('email')}}"/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'email'])
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">رقم الهاتف</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                        <input type="text" class="form-control" name="phone" value="{{isset($user) ? $user->phone : old('phone')}}"/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'phone'])
                                </div>
                            </div>

{{--                            @if(!$user)--}}
                                <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                                    <label class="col-md-3 col-xs-12 control-label"><span class="required">*</span> إسم الدخول</label>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-user-circle"></span></span>
                                            <input type="text" class="form-control" name="username" value="{{isset($user) ? $user->username : old('username')}}" {{isset($user) ? 'disabled' : 'required aria-required=""'}}/>
                                        </div>
                                        @include('admin.layouts.error', ['input' => 'username'])
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label class="col-md-3 col-xs-12 control-label"><span class="required">*</span> كلمة المرور</label>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                            <input type="password" class="form-control" name="password" @if(isset($user)) placeholder="إتركه فارغاً إذا لم ترد التعديل" @else required aria-required="" @endif/>
                                        </div>
                                        @include('admin.layouts.error', ['input' => 'password'])
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <label class="col-md-3 col-xs-12 control-label"><span class="required">*</span> تأكيد كلمة المرور</label>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                            <input type="password" class="form-control" name="password_confirmation" @if(isset($user)) placeholder="إتركه فارغاً إذا لم ترد التعديل" @else required aria-required="" @endif/>
                                        </div>
                                        @include('admin.layouts.error', ['input' => 'password_confirmation'])
                                    </div>
                                </div>
                            {{--@endif--}}

                            <div class="form-group {{ $errors->has('facebook_link') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">عنوان فيسبوك</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-facebook-official"></span></span>
                                        <input type="text" class="form-control" name="facebook_link" value="{{isset($user) ? $user->facebook_link : old('facebook_link')}}"/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'facebook_link'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('youtube_link') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">عنوان يوتيوب</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-youtube-square"></span></span>
                                        <input type="text" class="form-control" name="youtube_link" value="{{isset($user) ? $user->youtube_link : old('youtube_link')}}"/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'youtube_link'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('twitter_link') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">عنوان تويتر</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-twitter-square"></span></span>
                                        <input type="text" class="form-control" name="twitter_link" value="{{isset($user) ? $user->twitter_link : old('twitter_link')}}"/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'twitter_link'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('snapchat_link') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">عنوان سناب شات</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-snapchat"></span></span>
                                        <input type="text" class="form-control" name="snapchat_link" value="{{isset($user) ? $user->snapchat_link : old('snapchat_link')}}"/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'snapchat_link'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('insta_link') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">عنوان تويتر</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-instagram"></span></span>
                                        <input type="text" class="form-control" name="insta_link" value="{{isset($user) ? $user->insta_link : old('insta_link')}}"/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'insta_link'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الصورة الشخصية</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-image"></span></span>
                                        <a class="fileinput"><span></span><input class="fileinput" name="image" id="filename1" type="file"></a>
                                    </div>
                                    @if(isset($user))
                                        <br/>
                                        <span style="color: red;"> إتركه فارغاً إذا لم ترد التعديل </span>
                                        <br/>
                                        <img src="/users/{{$user->image}}" width="300px" height="300px" style="border: 1px solid #33414E;"/>
                                    @endif
                                    @include('admin.layouts.error', ['input' => 'image'])
                                </div>
                            </div>

                            @if(isset($user))
                                <input type="hidden" value="{{$user->id}}" name="user_id"/>
                            @endif
                        </form>
                        </div>
                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                                @if(isset($user))
                                    تعديل
                                @else
                                    إضافة
                                @endif
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
@endsection
