<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\Pack;
use App\Models\Service;
use App\Models\Subscription;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $users = User::where('role', 'user')->get()->count();
        $latest_users = User::where('role', 'user')->where('created_at', '>=', Carbon::today()->subWeek())->count();

        $celebs = User::where('role', 'celeb')->get()->count();
        $latest_celebs = User::where('role', 'celeb')->where('created_at', '>=', Carbon::today()->subWeek())->count();

        $services = Service::count();
        $packs = Pack::count();
        $subs = Subscription::count();
        $orders = Order::count();

        return view('admin.dashboard', compact('users','latest_users','celebs','latest_celebs','subs','services','packs','orders'));
    }
}
