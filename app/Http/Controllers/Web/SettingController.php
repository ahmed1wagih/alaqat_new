<?php

namespace App\Http\Controllers\Web;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    public function index()
    {
        $app_name = Setting::where('id', 1)->first()->value;
        $logo = Setting::where('id', 2)->first()->value;
        $icon = Setting::where('id', 3)->first()->value;
        $desc = Setting::where('id', 4)->first()->value;
        $facebook = Setting::where('id', 5)->first()->value;
        $twitter = Setting::where('id', 6)->first()->value;
        $instagram = Setting::where('id', 7)->first()->value;
    }
}
