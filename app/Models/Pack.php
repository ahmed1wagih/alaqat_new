<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pack extends Model
{
    protected $fillable = [
        'service_id','title','price','desc'
    ];


    public function subscriptions()
    {
        return $this->hasMany(Subscription::class, 'pack_id');
    }


    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }
}
