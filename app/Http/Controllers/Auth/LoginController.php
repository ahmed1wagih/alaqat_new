<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */


    public function user_login(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'username' => 'required',
                'password' => 'required'
            ]
        );

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'msg' => $validator->messages(), 'user_id' => '']);
        }

        $user = User::where('username', $request->username)->select('id', 'username', 'password')->first();

        if ($user) {
            $check = Hash::check($request->password, $user->password);

            if ($check)
            {
                return response()->json(['status' => 'success', 'msg' => 'logged in', 'user_id' => $user->id]);
            } else
            {
                return response()->json(['status' => 'failed', 'msg' => 'invalid data', 'user_id' => '']);
            }
        }
        else
        {
            return response()->json(['status' => 'failed', 'msg' => 'invalid data', 'user_id' => '']);
        }
    }


    public function admin_login_view()
    {
        return view('admin.login.login');
    }


    public function admin_login(Request $request)
    {
        $this->validate($request,
            [
                'email' => 'required|exists:users,email',
                'password' => 'required'
            ],
            [
                'email.required' => 'البريد الإلكتروني مطلوب',
                'email.exists' => 'البريد الإلكتروني غير صحيح',
                'password.required' => ';كلمة المرور مطلوبة',
            ]
        );

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password]))
        {
            return redirect('/admin/dashboard');
        }
        else
        {
            return back()->with('failed', 'بيانات خاطئة');
        }
    }


    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
