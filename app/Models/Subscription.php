<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = [
        'user_id', 'pack_id','status','expire_at'
    ];


    public function pack()
    {
        return $this->belongsTo(Pack::class, 'pack_id');
    }


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
