@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li class="active">المستخدمين </li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12 col-xs-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="/admin/user/create">
            <button type="button" class="btn btn-info">أضف مستخدم</button>
            </a>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="rtl_th">#</th>
                        <th class="rtl_th">البريد الإلكتروني</th>
                        <th class="rtl_th">الإسم</th>
                        <th class="rtl_th">إسم الدخول</th>
                        <th class="rtl_th">الصورة</th>
                        <th class="rtl_th">الرصيد</th>
                        <th class="rtl_th">الإجراء المتخذ</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{isset($user->email) ? $user->email : 'لا يوجد'}}</td>
                        <td>{{isset($user->name) ? $user->name : 'لا يوجد'}}</td>
                        <td>{{$user->username}}</td>
                        <td><img class="tiny_img" src="/users/{{$user->image}}"></td>
                        <td>{{$user->credit }}</td>
                        <td>
                                    <a href="/admin/user/{{$user->id}}/edit" title="تعديل" class="buttons"><button class="btn btn-warning btn-condensed"><i class="fa fa-eye"></i></button></a>
                                    <a href="/admin/user/{{$user->id}}/pack_assign" title="تعيين باقة" class="buttons"><button class="btn btn-success btn-condensed"><i class="fa fa-handshake-o"></i></button></a>
                                    <a href="/admin/user/{{$user->id}}/credit_assign" title="إضافة رصيد" class="buttons"><button class="btn btn-success btn-condensed"><i class="fa fa-dollar"></i></button></a>
                                    <form method="post" action="/admin/user/change_state" class="buttons">
                                        {{csrf_field()}}
                                        <input type="hidden" name="user_id" value="{{$user->id}}">
                                        @if($user->active== 0)
                                            <button class="btn btn-success btn-condensed" title="تفعيل"><i class="fa fa-thumbs-up"></i></button>
                                        @elseif($user->active == 1)
                                            <button class="btn btn-primary btn-condensed" title="إلغاء التفعيل"><i class="fa fa-thumbs-down"></i></button>
                                        @endif
                                    </form>
                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-warning-{{$user->id}}" title="حذف"><i class="fa fa-trash-o"></i></button>
                        </td>
                    </tr>
                    <!-- danger with sound -->
                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-warning-{{$user->id}}">
                        <div class="mb-container">
                            <div class="mb-middle warning-msg alert-msg">
                                <div class="mb-title"><span class="fa fa-times"></span> الرجاء الإنتباه</div>
                                <div class="mb-content">
                                   <p>أنت علي وشك أن تحذف هذا المستخدم و لن تستطيع إسترجاع بياناته مره أخري,مثل الإعلانات و العروض و الرسائل و ما إلي خلافه,هل أنت متأكد ؟</p>
                                </div>
                                <div class="mb-footer buttons">
                                    <form method="post" action="/admin/user/delete" class="buttons">
                                        {{csrf_field()}}
                                        <input type="hidden" name="user_id" value="{{$user->id}}">
                                        <button class="btn btn-default btn-lg pull-right">حذف</button>
                                    </form>
                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">إلغاء</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end danger with sound -->
                    @endforeach
                    </tbody>

                </table>
                {{$users->links()}}
            </div>
            </div>
            </div>
            </div>
        </div>
    </div>

@endsection
