<?php

namespace App\Http\Controllers\Admin;

use App\Models\Pack;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PackController extends Controller
{
    public function index($id)
    {
        $service = Service::find($id);
        $packs = Pack::where('service_id', $id)->paginate(20);

        return view('admin.services.packs.index', compact('service','packs'));
    }


    public function create($id)
    {
        $service = Service::find($id);
        return view('admin.services.packs.single', compact('service'));
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'service_id' => 'required|exists:services,id',
                'title' => 'required|unique:packs,title',
                'desc' => 'required',
                'price' => 'required',
            ],
            [
                'title.required' => 'عنوان الباقة مطلوب',
                'title.unique' => 'عنوان الباقة موجود من قبل',
                'desc.required' => 'وصف الباقة مطلوب',
                'price.required' => 'سعر الباقة مطلوب'
            ]
        );

        $pack = Pack::create
        (
            [
                'service_id' => $request->service_id,
                'title' => $request->title,
                'desc' => $request->desc,
                'price' => $request->price
            ]
        );

        return redirect('/admin/service/'.$pack->service_id.'/packs')->with('success', 'تم إضافة الباقة بنجاح');
    }


    public function edit($id)
    {
        $pack = Pack::find($id);
        return view('admin.services.packs.single', compact('pack'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'pack_id' => 'required|exists:packs,id',
                'title' => 'required|unique:packs,title,' . $request->pack_id,
                'desc' => 'required',
                'price' => 'required',
            ],
            [
                'title.required' => 'عنوان الباقة مطلوب',
                'title.unique' => 'عنوان الباقة موجود من قبل',
                'desc.required' => 'وصف الباقة مطلوب',
                'price.required' => 'سعر الباقة مطلوب'
            ]
        );

        $pack = Pack::find($request->pack_id);
                $pack->title = $request->title;
                $pack->desc = $request->desc;
                $pack->price = $request->price;
        $pack->save();

        return redirect('/admin/service/'.$pack->service_id .'/packs')->with('success', 'تم تعديل الباقة بنجاح');
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'pack_id' => 'required|exists:packs,id'
            ]
        );

        Pack::find('id', $request->pack_id)->delete();

        return back()->with('success', 'تمت الحذف بنجاح');
    }
}
