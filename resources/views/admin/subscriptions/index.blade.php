@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li>الإشتراكات</li>
        <li class="active">
            @if($status == 'approved')
                تم الموافقة عليها
            @elseif($status == 'awaiting')
                في إنتظار الموافقة
            @else
                مرفوضة
            @endif
        </li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12 col-xs-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="rtl_th">#</th>
                        <th class="rtl_th">إسم المستخدم</th>
                        <th class="rtl_th">البريد الإلكتروني</th>
                        <th class="rtl_th">الباقة</th>
                        <th class="rtl_th">الحالة</th>
                        <th class="rtl_th">النوع</th>
                        <th class="rtl_th">حتي</th>
                        <th class="rtl_th">الإجراء المتخذ</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($subs as $sub)
                    <tr>
                        <td>{{$sub->id}}</td>
                        <td>{{isset($sub->user->name) ? $sub->user->name : $sub->user->username}}</td>
                        <td>{{isset($sub->user->email) ? $sub->user->email : 'لا يوجد'}}</td>
                        <td>{{$sub->pack->service->title}} - {{$sub->pack->title}}</td>
                        <td>
                            @if($sub->status == 'approved')
                                تمت الموافقة
                            @elseif($sub->status == 'awaiting')
                                في الإنتظار
                            @else
                                مرفوض
                            @endif
                        </td>
                        <td>
                            @if($sub->type == 'recipt')
                               فاتورة
                            @else
                                رصيد
                            @endif
                        </td>
                        <td>
                            {{isset($sub->expire_at) ? $sub->expire_at : 'غير محدد'}}
                        </td>
                        <td>
                            @if($sub->status != 'approved')
                                <form method="post" action="/admin/subscription/change_state" class="buttons">
                                    {{csrf_field()}}
                                    <input type="hidden" name="sub_id" value="{{$sub->id}}">
                                    <input type="hidden" name="status" value="approved">
                                    <button class="btn btn-success btn-condensed" title="موافقة"><i class="fa fa-check-square"></i></button>
                                </form>
                            @endif
                            <form method="post" action="/admin/subscription/change_state" class="buttons">
                                {{csrf_field()}}
                                <input type="hidden" name="sub_id" value="{{$sub->id}}">
                                <input type="hidden" name="status" value="declined">
                                <button class="btn btn-danger btn-condensed" title="رفض"><i class="fa fa-minus-circle"></i></button>
                            </form>
                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-warning-{{$sub->id}}" title="حذف"><i class="fa fa-trash-o"></i></button>
                        </td>
                    </tr>
                    <!-- danger with sound -->
                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-warning-{{$sub->id}}">
                        <div class="mb-container">
                            <div class="mb-middle warning-msg alert-msg">
                                <div class="mb-title"><span class="fa fa-times"></span> الرجاء الإنتباه</div>
                                <div class="mb-content">
                                   <p>أنت علي وشك أن تحذف هذا الإشتراك و لن تستطيع إسترجاع بياناته مره أخري,هل أنت متأكد ؟</p>
                                </div>
                                <div class="mb-footer buttons">
                                    <form method="post" action="/admin/subscription/delete" class="buttons">
                                        {{csrf_field()}}
                                        <input type="hidden" name="sub_id" value="{{$sub->id}}">
                                        <button class="btn btn-default btn-lg pull-right">حذف</button>
                                    </form>
                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">إلغاء</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end danger with sound -->
                    @endforeach
                    </tbody>

                </table>
                {{$subs->links()}}
            </div>
            </div>
            </div>
            </div>
        </div>
    </div>

@endsection
