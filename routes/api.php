<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'Auth\LoginController@user_login');

Route::get('/home', 'Api\HomeController@index');
Route::get('/service_details', 'Api\HomeController@service_details');
Route::get('/get_celebs', 'Api\HomeController@get_celebs');

Route::post('/order', 'Api\HomeController@order');

Route::post('/celebs_search', 'Api\HomeController@celebs_search');
Route::get('/profile/{profile_id}/{user_id}', 'Api\HomeController@profile');

Route::post('/profile/update', 'Api\HomeController@update_profile');

Route::post('/subscribe', 'Api\HomeController@subscribe');
