<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    public function index()
    {
        $settings = Setting::get();
        return view('admin.settings.index', compact('settings'));
    }


    public function edit($id)
    {
        $setting = Setting::find($id);
        return view('admin.settings.single', compact('setting'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'setting_id' => 'required|exists:settings,id',
                'value' => 'required'
            ],
            [
                'value.required' => 'الرجاء إدخال قيمة للإعداد'
            ]
        );

        $setting = Setting::find($request->setting_id);
            if($request->setting_id == 2 || $request->setting_id == 3)
            {
                $image = unique_file($request->value->getClientOriginalName());
                $request->value->move(base_path().'/public/settings/',$image);
                $setting->value = $image;
            }
            else
            {
                $setting->value = $request->value;
            }
        $setting->save();

        return redirect('/admin/settings')->with('success', 'تم التعديل بنجاح');
    }
}
