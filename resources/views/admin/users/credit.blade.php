@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li><a>مستخدمين التطبيق</a></li>
        <li class="active">
            إضافة رصيد الرصيد
        </li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        @include('admin.layouts.message')
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="/admin/user/credit_assign">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <strong>
                                  إضافة رصيد
                                </strong>
                            </h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-group {{ $errors->has('credit') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label"><span class="required">*</span>الرصيد الحالي</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-dollar"></span></span>
                                        <input type="number" class="form-control" name="credit" required aria-required="" value="{{$user->credit}}">
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'credit'])
                                </div>
                            </div>

                            <input type="hidden" value="{{$user->id}}" name="user_id"/>

                </form>
            </div>
            <div class="panel-footer">
                <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                <button class="btn btn-primary pull-right">
                    إضافة
                </button>
            </div>
        </div>
        </form>

    </div>
    </div>

    </div>

@endsection
