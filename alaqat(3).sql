-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 26, 2018 at 05:30 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alaqat`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `celebs` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `from` date NOT NULL,
  `to` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event_images`
--

DROP TABLE IF EXISTS `event_images`;
CREATE TABLE IF NOT EXISTS `event_images` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_10_14_224728_create_events_table', 1),
(4, '2018_10_14_224959_create_event_images_table', 1),
(5, '2018_10_14_225042_create_services_table', 1),
(6, '2018_10_14_225409_create_packs_table', 1),
(7, '2018_10_14_230135_create_subscriptions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` enum('approved','awaiting','declined') NOT NULL DEFAULT 'awaiting',
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `desc` mediumtext NOT NULL,
  `celebs_ids` text NOT NULL,
  `from` date NOT NULL,
  `to` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `status`, `user_id`, `name`, `desc`, `celebs_ids`, `from`, `to`, `created_at`, `updated_at`) VALUES
(1, 'approved', 3, 'Ahmed Ahmed', 'عاوز أعمل دعاية لإفتتاح المحل بتاعي و عاوز الناس دي', 'a:3:{i:0;s:1:\"5\";i:1;s:1:\"6\";i:2;s:1:\"8\";}', '2018-12-02', '2018-12-04', '2018-10-16 18:58:41', '2018-10-16 20:17:25'),
(2, 'awaiting', 3, 'Ahmed Ahmed', 'عاوز أعمل الفرح بتاعي و عاوزي المغنيين دول', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"8\";}', '2018-12-02', '2018-12-04', '2018-10-16 18:59:50', '2018-10-16 18:59:50'),
(3, 'awaiting', 3, 'Ahmed Ahmed', 'عاوز أعمل الفرح بتاعي و عاوزي المغنيين دول  تاني', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"8\";}', '2018-12-02', '2018-12-04', '2018-10-16 20:20:04', '2018-10-16 20:20:04'),
(4, 'awaiting', 3, 'Ahmed Ahmed', 'عاوز أعمل الفرح بتاعي و عاوزي المغنيين دول  تالت', 'a:2:{i:0;s:1:\"6\";i:1;s:1:\"8\";}', '2018-12-02', '2018-12-04', '2018-10-16 20:21:51', '2018-10-16 20:21:51');

-- --------------------------------------------------------

--
-- Table structure for table `order_images`
--

DROP TABLE IF EXISTS `order_images`;
CREATE TABLE IF NOT EXISTS `order_images` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(11) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_images`
--

INSERT INTO `order_images` (`id`, `order_id`, `image`, `created_at`, `updated_at`) VALUES
(1, 1, '15397235215bc651013f6a130516307_1892163197483148_7196346580023977958_n.jpg', '2018-10-16 18:58:41', '2018-10-16 18:58:41'),
(2, 1, '15397235215bc651014f7b3enhanced-19898-1425418851-9.jpg', '2018-10-16 18:58:41', '2018-10-16 18:58:41'),
(3, 2, '15397235905bc65146274b715186_383382068442634_875540377_n.jpg', '2018-10-16 18:59:50', '2018-10-16 18:59:50'),
(4, 2, '15397235905bc651465a6a731052122_1902512693114865_1355982624331403966_n.jpg', '2018-10-16 18:59:50', '2018-10-16 18:59:50'),
(5, 3, '15397284045bc664140ccad15186_383382068442634_875540377_n.jpg', '2018-10-16 20:20:04', '2018-10-16 20:20:04'),
(6, 3, '15397284045bc664142278b31052122_1902512693114865_1355982624331403966_n.jpg', '2018-10-16 20:20:04', '2018-10-16 20:20:04'),
(7, 4, '15397285115bc6647f1abd915186_383382068442634_875540377_n.jpg', '2018-10-16 20:21:51', '2018-10-16 20:21:51'),
(8, 4, '15397285115bc6647f531a831052122_1902512693114865_1355982624331403966_n.jpg', '2018-10-16 20:21:51', '2018-10-16 20:21:51');

-- --------------------------------------------------------

--
-- Table structure for table `packs`
--

DROP TABLE IF EXISTS `packs`;
CREATE TABLE IF NOT EXISTS `packs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(255) NOT NULL,
  `color` int(2) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packs`
--

INSERT INTO `packs` (`id`, `service_id`, `title`, `desc`, `price`, `color`, `created_at`, `updated_at`) VALUES
(1, 1, 'الحزمة الذهبية', 'وصف الباقة <br>', 600, 1, '2018-10-15 18:29:56', '2018-10-15 18:34:41'),
(2, 1, 'الباقة الفضية', '<p>تفاصيل الباقة<br></p>', 400, 2, '2018-10-15 18:36:29', '2018-10-15 18:36:29'),
(3, 1, 'الباقة البرونزية', '<p>تفاصيل الباقة<br></p><br>', 300, 3, '2018-10-15 18:36:53', '2018-10-15 18:36:53');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expire_at` timestamp NOT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` int(2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `desc`, `details`, `image`, `color`, `created_at`, `updated_at`) VALUES
(1, 'الحلول الإعلامية المتكاملة', 'هنالك ما يعكس صورتك الفنية الإيجابية بلوحة إبداعية جميلة الملامح', '<ul><li>العامل الأول</li><li>العامل الثاني</li><li>العامل الثالث</li><li>العامل الرابع</li><li>العامل الخامس<br></li></ul>', '15403026735bcf2751272afdefault .png', 1, '2018-10-15 17:49:32', '2018-10-23 11:51:13'),
(2, 'الإستشارات', '{{ ما خاب من إستشار }} نضع بين يديك حلاً و إستشارة تحقق غايتك', '<ul><li>العامل الأول</li><li>العامل الثاني</li><li>العامل الثالث</li><li>العامل الرابع</li><li>العامل الخامس<br></li></ul>', '15403026195bcf271bcd1bbadministrator-30011.jpg', 3, '2018-10-15 17:53:02', '2018-10-23 11:50:19'),
(3, 'العلاقات العامة', 'علاقتك بأصحاب الإختصاص من أقصر طرق الوصول إلي مبتغاك, تعهدنا بأن نصلك بهم', '<ul><li>العامل الأول</li><li>العامل الثاني</li><li>العامل الثالث</li><li>العامل الرابع</li><li>العامل الخامس<br></li></ul>', '15403026045bcf270c4a97alhr-men-fb.jpg', 4, '2018-10-15 17:54:15', '2018-10-23 11:50:04'),
(4, 'رعاية', 'وصف وصف وصف', '<p>تفاصيل تفاصيل</p>', '15403024885bcf26980f462abstraction_line_stains_122371_1024x768.jpg', 2, '2018-10-23 11:48:08', '2018-10-23 11:48:08');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`) VALUES
(1, 'app_name', 'علاقات للخدمات الإعلانية'),
(2, 'logo', '15398075475bc7993bc6216Alaqat logo-01.png'),
(3, 'icon', '15398075555bc79943d6c0dAlaqat logo-01.png'),
(4, 'desc', '<span style=\"font-weight: bold;\">وصف وصف وصف وصف وصف وصف وصف<br></span>'),
(5, 'facecbook', 'https://www.facebook.com/alaqat'),
(6, 'twitter', 'https://www.twitter.com/alaqat'),
(7, 'youtube', 'https://www.youtube.com/alaqat');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE IF NOT EXISTS `subscriptions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` enum('recipt','credit') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('approved','awaiting','declined') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'awaiting',
  `user_id` int(11) NOT NULL,
  `pack_id` int(11) NOT NULL,
  `expire_at` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `type`, `status`, `user_id`, `pack_id`, `expire_at`, `created_at`, `updated_at`) VALUES
(1, 'recipt', 'approved', 2, 1, NULL, '2018-10-16 19:09:01', '2018-10-16 19:37:02'),
(3, 'recipt', 'approved', 4, 1, '2019-01-18', '2018-10-23 11:17:53', '2018-10-26 15:04:56'),
(4, 'recipt', 'approved', 3, 1, NULL, '2018-10-23 11:18:18', '2018-10-23 11:18:18'),
(5, 'recipt', 'approved', 3, 3, '2019-03-03', '2018-10-23 11:19:33', '2018-10-23 11:20:31');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role` enum('admin','celeb','user') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `credit` double NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` mediumtext COLLATE utf8mb4_unicode_ci,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.png',
  `facebook_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `credit`, `active`, `name`, `title`, `desc`, `email`, `phone`, `username`, `password`, `image`, `facebook_link`, `youtube_link`, `twitter_link`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 0, 1, 'Admin', 'Admin', 'desc', 'admin@admin.com', '', '', '$2y$10$vpL09pRgQNMLwrBWxeB3COy2qcRZ6iQiIxEnxRc15E2LWhHWnEJqG', 'default.png', NULL, NULL, NULL, 'igEpGNyBiq46QD2vJ59Cd57maEuNTu2zo3rxOIVWTiEUSKJpSi6KT1zCuzS8', '2018-10-01 22:00:00', '2018-10-14 22:00:00'),
(2, 'user', 10000, 1, NULL, NULL, NULL, NULL, NULL, 'username1', '$2y$10$vpL09pRgQNMLwrBWxeB3COy2qcRZ6iQiIxEnxRc15E2LWhHWnEJqG', 'default.png', NULL, NULL, NULL, NULL, '2018-10-15 16:38:50', '2018-10-26 15:12:53'),
(3, 'user', 0, 1, NULL, NULL, NULL, NULL, NULL, 'ahmed3242', '$2y$10$vpL09pRgQNMLwrBWxeB3COy2qcRZ6iQiIxEnxRc15E2LWhHWnEJqG', 'default.png', NULL, NULL, NULL, NULL, '2018-10-15 16:44:03', '2018-10-15 16:44:03'),
(4, 'user', 600, 1, NULL, NULL, NULL, NULL, NULL, 'mo_mo33', '$2y$10$vpL09pRgQNMLwrBWxeB3COy2qcRZ6iQiIxEnxRc15E2LWhHWnEJqG', '15402978105bcf1452dff30eagle.PNG', NULL, NULL, NULL, NULL, '2018-10-15 16:44:59', '2018-10-26 15:04:56'),
(5, 'celeb', 0, 1, 'Tamer Hosney', 'Singer & Actor', NULL, 'tamer@tamer.com', '0123654684', 'tamertamer', '$2y$10$vpL09pRgQNMLwrBWxeB3COy2qcRZ6iQiIxEnxRc15E2LWhHWnEJqG', '15396292215bc4e0a5de588.jpg', NULL, NULL, NULL, NULL, '2018-10-15 16:47:01', '2018-10-15 16:47:01'),
(6, 'celeb', 0, 1, 'Karoll Samaha', 'Singer', NULL, 'samaha@samaha.com', NULL, 'Karoll Samaha', '$2y$10$vpL09pRgQNMLwrBWxeB3COy2qcRZ6iQiIxEnxRc15E2LWhHWnEJqG', '15396294175bc4e1690177b.jpg', NULL, NULL, NULL, NULL, '2018-10-15 16:50:17', '2018-10-15 16:50:17'),
(8, 'celeb', 0, 1, 'Amr Diab', 'Singer', NULL, NULL, NULL, 'diaboo2', '$2y$10$vpL09pRgQNMLwrBWxeB3COy2qcRZ6iQiIxEnxRc15E2LWhHWnEJqG', '15396295755bc4e207188ce.jpg', NULL, NULL, NULL, NULL, '2018-10-15 16:52:55', '2018-10-15 16:52:55'),
(9, 'celeb', 0, 1, 'أحمد حلمي', 'Actor', NULL, NULL, NULL, 'helmy_helmy', '$2y$10$vpL09pRgQNMLwrBWxeB3COy2qcRZ6iQiIxEnxRc15E2LWhHWnEJqG', '15396297065bc4e28aed693.jpg', NULL, NULL, NULL, NULL, '2018-10-15 16:55:06', '2018-10-15 16:55:06'),
(10, 'celeb', 0, 1, 'Ahmed Wagih', 'Php Developer', NULL, NULL, NULL, 'ahmedwagih', '$2y$10$vpL09pRgQNMLwrBWxeB3COy2qcRZ6iQiIxEnxRc15E2LWhHWnEJqG', '15396297885bc4e2dc8e7f6.jpg', NULL, NULL, NULL, NULL, '2018-10-15 16:56:28', '2018-10-15 17:01:27');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `order_images`
--
ALTER TABLE `order_images`
  ADD CONSTRAINT `order_images_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
