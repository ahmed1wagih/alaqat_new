@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li> <a href="/admin/services">الباقات</a></li>
        <li>{{$service->title}}</li>
        <li class="active">الباقات</li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12 col-xs-12">
            @include('admin.layouts.message')
            <!-- START BASIC TABLE SAMPLE -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <a href="/admin/pack/{{$service->id}}/create">
            <button type="button" class="btn btn-info">أضف باقة</button>
            </a>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="rtl_th">العنوان</th>
                        <th class="rtl_th">السعر</th>
                        <th class="rtl_th">الوصف</th>
                        <th class="rtl_th">عدد المشتركين</th>
                        <th class="rtl_th">الإجراء المتخذ</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($packs as $pack)
                    <tr>
                        <td>{{$pack->title}}</td>
                        <td>{{$pack->price}}</td>
                        <td>{!! $pack->desc !!}</td>
                        <td>{{$pack->subscriptions->count()}}</td>
                        <td>
                            <a href="/admin/pack/{{$pack->id}}/edit" title="تعديل" class="buttons"><button class="btn btn-warning btn-condensed"><i class="fa fa-edit"></i></button></a>
                            <button class="btn btn-danger btn-condensed mb-control" data-box="#message-box-warning-{{$pack->id}}" title="حذف"><i class="fa fa-trash-o"></i></button>
                        </td>
                    </tr>
                    <!-- danger with sound -->
                    <div class="message-box message-box-danger animated fadeIn" data-sound="alert/fail" id="message-box-warning-{{$service->id}}">
                        <div class="mb-container">
                            <div class="mb-middle warning-msg alert-msg">
                                <div class="mb-title"><span class="fa fa-times"></span> الرجاء الإنتباه</div>
                                <div class="mb-content">
                                   <p>أنت علي وشك أن تحذف هذه الباقة و لن تستطيع إسترجاع بياناتها مره أخري,مثل الإشتراكات و ما إلي خلافه,هل أنت متأكد ؟</p>
                                </div>
                                <div class="mb-footer buttons">
                                    <form method="post" action="/admin/pack/delete" class="buttons">
                                        {{csrf_field()}}
                                        <input type="hidden" name="pack_id" value="{{$pack->id}}">
                                        <button class="btn btn-default btn-lg pull-right">حذف</button>
                                    </form>
                                    <button class="btn btn-default btn-lg pull-right mb-control-close" style="margin-right: 5px;">إلغاء</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end danger with sound -->
                    @endforeach
                    </tbody>

                </table>
                {{$packs->links()}}
            </div>
            </div>
            </div>
            </div>
        </div>
    </div>

@endsection
