@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li><a href="/admin/services">الخدمات</a></li>
        <li class="active">
            @if(isset($service))
                تعديل خدمة
            @else
                إضافة خدمة
            @endif
        </li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" @if(isset($service)) action="/admin/service/update" @else action="/admin/service/store" @endif enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <strong>
                                   @if(isset($service))
                                        تعديل خدمة
                                    @else
                                        إضافة خدمة
                                   @endif
                                </strong>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">العنوان</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="title" value="{{isset($service) ? $service->title : old('title')}}"/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'title'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('desc') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الوصف</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="text" class="form-control" name="desc" value="{{isset($service) ? $service->desc : old('desc')}}"/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'desc'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('details') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">التفاصيل</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <textarea class="summernote" name="details" value="{{old('details')}}">{{isset($service) ? $service->details : old('details')}}</textarea>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'details'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">الصورة</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-image"></span></span>
                                        <a class="fileinput"><span></span><input class="fileinput" name="image" id="filename1" type="file"></a>
                                    </div>

                                    @if(isset($service))
                                        <br/>
                                        <span style="color: red;"> إتركه فارغاً إذا لم ترد التعديل </span>
                                        <br/>
                                        <img src="/services/{{$service->image}}" width="300px" height="300px" style="border: 1px solid #33414E;"/>
                                    @endif

                                    @include('admin.layouts.error', ['input' => 'image'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('desc') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">اللون (من 1 إلي 20 )</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-eyedropper"></span></span>
                                        <input type="number" class="form-control" name="color" value="{{isset($service) ? $service->color : old('color')}}"/>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'desc'])
                                </div>
                            </div>
                            @if(isset($service))
                            <input type="hidden" name="service_id" value="{{$service->id}}">
                            @endif
                </form>
                        </div>
                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                                @if(isset($service))
                                    تعديل
                                @else
                                    إضافة
                                @endif
                            </button>
                        </div>
                    </div>
    </div>

    <!-- THIS PAGE PLUGINS -->
    <script type='text/javascript' src="{{asset('admin/js/plugins/icheck/icheck.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/summernote/summernote.js')}}"></script>
    <!-- END PAGE PLUGINS -->


@endsection
