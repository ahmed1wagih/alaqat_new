<?php

namespace App\Http\Controllers\Admin;

use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    public function index()
    {
        $services = Service::paginate(20);
        return view('admin.services.index', compact('services'));
    }


    public function create()
    {
        return view('admin.services.single');
    }


    public function store(Request $request)
    {
        $this->validate($request,
            [
                'title' => 'required|unique:services,title',
                'desc' => 'required',
                'details' => 'required',
                'image' => 'required|image',
                'color' => 'required'
            ],
            [
                'title.required' => 'عنوان الخدمة مطلوب',
                'title.unique' => 'وصف الخدمة مطلوب',
                'desc.required' => 'وصف الخدمة مطلوب',
                'details.required' => 'تفاصيل الخدمة مطلوبة',
                'image.required' => 'الصورة مطلوبة',
                'image.image' => 'صورة غير صحيحة',
                'color.required' => 'اللون مطلوب',
            ]
        );

        $image = unique_file($request->image->GetClientOriginalname());
        $request->image->move(base_path().'/public/services/', $image);

        Service::create
        (
            [
                'title' => $request->title,
                'desc' => $request->desc,
                'details' => $request->details,
                'image' => $image,
                'color' => $request->color
            ]
        );

        return redirect('/admin/services')->with('success', 'تمت الإضافة بنجاح');
    }


    public function edit($id)
    {
        $service = Service::find($id);
        return view('admin.services.single', compact('service'));
    }


    public function update(Request $request)
    {
        $this->validate($request,
            [
                'service_id' => 'required|exists:services,id',
                'title' => 'required|unique:services,title,'.$request->service_id,
                'desc' => 'required',
                'details' => 'required',
                'image' => 'sometimes|image',
                'color' => 'required'
            ],
            [
                'title.required' => 'عنوان الخدمة مطلوب',
                'title.unique' => 'وصف الخدمة مطلوب',
                'desc.required' => 'وصف الخدمة مطلوب',
                'details.required' => 'تفاصيل الخدمة مطلوبة',
                'image.image' => 'صورة غير صحيحة',
                'color.required' => 'اللون مطلوب',
            ]
        );


        Service::where('id', $request->service_id)->update
        (
            [
                'title' => $request->title,
                'desc' => $request->desc,
                'details' => $request->details,
                'color' => $request->color
            ]
        );

        if($request->image)
        {
            $image = unique_file($request->image->GetClientOriginalname());
            $request->image->move(base_path().'/public/services/', $image);

            $service = Service::find($request->service_id);
                $service->image = $image;
            $service->save();
        }

        return redirect('/admin/services')->with('success', 'تم التعديل بنجاح');
    }


    public function destroy(Request $request)
    {
        $this->validate($request,
            [
                'service_id' => 'required|exists:services,id'
            ]
        );

        Service::where('id', $request->servie_id)->delete();

        return back()->with('success', 'تمت الحذف بنجاح');
    }
}
