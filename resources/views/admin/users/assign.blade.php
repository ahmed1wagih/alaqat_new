@extends('admin.layouts.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">الرئيسية</a></li>
        <li><a>مستخدمين التطبيق</a></li>
        <li class="active">
           تعيين باقة
        </li>
    </ul>
    <!-- END BREADCRUMB -->
    <div class="page-content-wrap">
        @include('admin.layouts.message')
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" method="post" action="/admin/user/pack_assign"enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <strong>
                                    تعيين باقة
                                </strong>
                            </h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label"><span class="required">*</span> الخدمة</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                            <select class="form-control select" id="service" required aria-required="">
                                                <option disabled selected>إختر نوع الخدمة</option>
                                                @foreach($services as $service)
                                                    <option value="{{$service->id}}">{{$service->title}}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('pack_id') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label"><span class="required">*</span>الباقة</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <select class="form-control" name="pack_id" id="packs" required aria-required="">
                                            <option disabled selected>إختر خدمة أولاً</option>
                                        </select>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'pack_id'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('added_by') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label"><span class="required">*</span>نوع التعيين</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-dollar"></span></span>
                                        <select class="form-control select" name="type" required aria-required="">
                                            <option disabled selected>إختر النوع</option>
                                            <option value="recipt">فاتورة</option>
                                            <option value="credit">خصم من الرصيد</option>
                                        </select>
                                    </div>
                                    @include('admin.layouts.error', ['input' => 'pack_id'])
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
                                <label class="col-md-3 col-xs-12 control-label">حتي </label>
                                <div class="col-md-6 col-xs-12">

                                    <div class="input-group" id="till">
                                        <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                        <input type="date" class="form-control" name="date" value=""/>
                                    </div>
                                    <br/>
                                    <label class="switch switch-small">
                                        <input type="checkbox" name="no_date" id="no_date">
                                        <span></span>
                                    </label>
                                    <label type="label">مدة غير محدودة ؟ </label>


                                    @include('admin.layouts.error', ['input' => 'date'])
                                </div>
                            </div>

                            <input type="hidden" value="{{$user->id}}" name="user_id"/>

                        </form>
                        </div>
                        <div class="panel-footer">
                            <button type="reset" class="btn btn-default">تفريغ</button> &nbsp;
                            <button class="btn btn-primary pull-right">
                               تعيين
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>

    <script>
        $('#service').on('change', function (e) {

            var parent = e.target.value;

            $.ajax({
                url: '/admin/get_packs/' + parent,
                type: "GET",

                dataType: "json",

                success: function (data) {
                    $('#packs').empty();
                    $('#packs').append('<option selected disabled> إختر الباقة </option>');
                    $.each(data, function (i, pack) {
                        $('#packs').append('<option value="' + pack.id + '">' + pack.title + '</option>');
                    });

                }
            });
        })


        $('#no_date').on('click', function(){

            if($('#till').css('display') == 'none'){
                $('#till').show('slow');
            } else {
                $('#till').hide('slow');
            }
        });
    </script>
@endsection
