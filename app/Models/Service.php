<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'title', 'desc', 'details','image','color'
    ];


    public function packs()
    {
        return $this->hasMany(Pack::class, 'service_id');
    }


    public function check_status($service_id,$user_id)
    {
        $subs = Subscription::where('user_id', $user_id)->select('pack_id','status')->get();

        if($subs->count() > 0)
        {
            foreach($subs as $sub)
            {
                if($sub->pack->service->id == $service_id)
                {
                    return $sub['status'];
                }
                else
                {
                    return 'not_applied';
                }
            }
        }
        else
        {
            return 'not_applied';
        }

    }
}
