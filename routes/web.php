<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\IsAdmin;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', 'Auth\LoginController@admin_login_view');
Route::post('/login', 'Auth\LoginController@admin_login');
Route::get('/logout', 'Auth\LoginController@logout');

Route::group(['middleware' => ['web', IsAdmin::class]], function () {
    Route::group(['prefix' => '/admin'], function () {
        Route::get('/dashboard', 'Admin\HomeController@index');

        Route::get('/users/admins', 'Admin\UserController@admins');
        Route::get('/users/celebs', 'Admin\UserController@celebs');
        Route::get('/users/users', 'Admin\UserController@users');
        Route::get('/users/blocked', 'Admin\UserController@blocked');
        Route::get('/user/create', 'Admin\UserController@create');
        Route::post('/user/store', 'Admin\UserController@store');
        Route::get('/user/{id}/edit', 'Admin\UserController@edit');
        Route::post('/user/update', 'Admin\UserController@update');
        Route::post('/user/change_state', 'Admin\UserController@change_state');
        Route::get('/user/{id}/pack_assign', 'Admin\UserController@pack_assign_view');
        Route::get('/user/{id}/credit_assign', 'Admin\UserController@credit_assign_view');
        Route::post('/user/pack_assign', 'Admin\UserController@pack_assign');
        Route::post('/user/credit_assign', 'Admin\UserController@credit_assign');
        Route::post('/user/delete', 'Admin\UserController@destroy');

        Route::get('/services', 'Admin\ServiceController@index');
        Route::get('/service/create', 'Admin\ServiceController@create');
        Route::post('/service/store', 'Admin\ServiceController@store');
        Route::get('/service/{id}/edit', 'Admin\ServiceController@edit');
        Route::post('/service/update', 'Admin\ServiceController@update');
        Route::post('/service/delete', 'Admin\ServiceController@destroy');

        Route::get('/service/{service_id}/packs', 'Admin\PackController@index');
        Route::get('/pack/{service_id}/create', 'Admin\PackController@create');
        Route::post('/pack/store', 'Admin\PackController@store');
        Route::get('/pack/{id}/edit', 'Admin\PackController@edit');
        Route::post('/pack/update', 'Admin\PackController@update');
        Route::post('/pack/delete', 'Admin\PackController@destroy');

        Route::get('/subscriptions/{status}', 'Admin\SubscriptionController@index');
        Route::post('/subscription/change_state', 'Admin\SubscriptionController@change_state');
        Route::post('/subscription/delete', 'Admin\SubscriptionController@destroy');

        Route::get('/orders/{status}', 'Admin\OrderController@index');
        Route::post('/order/change_state', 'Admin\OrderController@change_state');
        Route::post('/order/delete', 'Admin\OrderController@destroy');

        Route::get('/settings', 'Admin\SettingController@index');
        Route::get('/setting/{id}/edit', 'Admin\SettingController@edit');
        Route::post('/setting/update', 'Admin\SettingController@update');

        Route::get('/get_packs/{id}', 'Admin\UserController@get_packs');
    });
});


//user/view
//user/update
